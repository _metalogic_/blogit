module bitbucket.org/_metalogic_/blogit

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.1.0
	github.com/alecthomas/chroma v0.7.0
	github.com/andybalholm/cascadia v0.0.0-20161224141413-349dd0209470 // indirect
	github.com/beevik/etree v1.1.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/russross/blackfriday/v2 v2.0.1
	github.com/shurcooL/sanitized_anchor_name v0.0.0-20170515013256-541ff5ee47f1 // indirect
	golang.org/x/net v0.0.0-20170605033737-59a0b19b5533 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/yaml.v2 v2.0.0-20170407172122-cd8b52f8269e
)
